/*
dipped metal film caps MMR series
Illinois capacitor

Usage:
`MMRCapacitor(...dimensions)`
*/
$fn = 64;


// mm
// L length
// H height
// T thickness
// S lead spacing
// ll lead length
// d lead diameter


module MMRCapacitor(L, H, T, S, ll, d) {

  module lead(d, ll) {
    cylinder(h=ll, r=d/2,center=true);
  }

  module leads() {
    translate([0, 0, -H]) {
      translate([S/2,0, 0])
        lead(d, ll);
      translate([-S/2, 0, 0])
        lead(d, ll);
    }
  }

  module body() {
    hullRadius = T/2;
    points = [
      [-(L/2)+hullRadius, -(T/2)+hullRadius, (H/2)-hullRadius],
      [(L/2)-hullRadius, 0, (H/2)-hullRadius],
      [(L/2)-hullRadius, 0, -(H/2)+hullRadius],
      [-(L/2)+hullRadius, 0, -(H/2)+hullRadius]
    ];

    color("#733125") {
      hull() {
        for (p = points) {
          translate (p) sphere(r=hullRadius);
        }
      }
    }
  }

  body();
  leads();
};
