# MMR Series OpenScad Models

Illinois Capacitor (Cornell-Dubilier) epoxy-dipped, metal-film capacitor 3D
solid models, from [OpenSCAD](https://openscad.org/).

## Usage

Drop your dimensions into a module invocation:

```
MMRCapacitor(L, H, T, S, ll, d)
```

Where:
* `L` total length
* `H` height
* `T` thickness
* `S` lead spacing
* `ll` lead length
* `d` diameter

## Datasheet

[Wayback archive 2022-04-10](https://web.archive.org/web/20220410130031/https://www.cde.com/resources/catalogs/MMR.pdf)

## Implementation Notes

This model uses `hulls`, which, as `minkowski`s, are not amenable to
tesselation in all CAD environments. This may not affect you, but if it does,
many packages exist to convert to whatever format your EDA accepts.

## License

* © Source code daemonburrito 2022
* © Design of capacitor and linked datasheet copyright CDE/IC

All source code licensed under the AGPL 3.0 or greater.

## Notice of Merchantability and Fitness

Source Code Copyright © 2022 daemonburrito

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
