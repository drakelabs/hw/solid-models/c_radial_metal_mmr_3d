use <../../lib/MMR.scad>
/*
dipped metal film caps MMR series
Illinois capacitor
*/
$fn = 64;

// L length
L=12;
// H height
H=7.5;
// T thickness
T=4.5;
// S lead spacing
S=10;
// ll lead length
ll=12;
// d lead diameter
d=0.6;

MMRCapacitor(L,H,T,S,ll,d);
