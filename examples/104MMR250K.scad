use <../../lib/MMR.scad>
/*
dipped metal film caps MMR series
Illinois capacitor
*/
$fn = 64;

// L length
L=10.3;
// H height
H=8.4;
// T thickness
T=5.8;
// S lead spacing
S=7.5;
// ll lead length
ll=15;
// d lead diameter
d=0.7;

MMRCapacitor(L,H,T,S,ll,d);
