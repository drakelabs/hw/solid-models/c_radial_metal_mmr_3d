use <../../lib/MMR.scad>
/*
dipped metal film caps MMR series
Illinois capacitor
*/
$fn = 64;

// L length
L=10.3;
// H height
H=7.5;
// T thickness
T=4.5;
// S lead spacing
S=7.5;
// ll lead length
ll=14;
// d lead diameter
d=0.6;

MMRCapacitor(L,H,T,S,ll,d);
